# Blue Donut

Blue Donut is a Django website to make a few annoying tasks in EVE Online easier for members of the Legacy Coalition.
It's main purpose is route planning using the expansive Ansiblex Jump Gate network in our space.

So far I haven't included any tests or CI. There are plans to incorporate everything, but for now we just have to deal with it.

Made with inspiration from Sharad Heft's [website](https://github.com/StephenSwat/eve-abyssal-market).

## Installation

Figure it out.

## Contributing

Make a pull request or issue and I will get to it eventually. This is a learning experience for me.
